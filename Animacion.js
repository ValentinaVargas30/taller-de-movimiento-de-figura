//Crea una escena
//THREE
let escena=new THREE.Scene();
 //Crear la camara 
let camara=new THREE.PerspectiveCamera(100, window.innerWidth/window.innerHeight, 0.1, 1000);

//Crear lienzo (renderer) 
let lienzo= new THREE.WebGLRenderer();
lienzo.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(lienzo.domElement);

//Crear geometry 
let cuboide= new THREE.BoxGeometry();
let material= new THREE.MeshBasicMaterial({color:0x1071A1});

//Crear la malla
let miCubo= new THREE.Mesh(cuboide, material);

//Agregar "la malla" a la escena 
escena.add(miCubo);

camara.position.z=5;

let animar = function(){

    requestAnimationFrame(animar);
    miCubo.rotation.x=miCubo.rotation.x+0.02;
    miCubo.rotation.y=miCubo.rotation.y+0.02;
    lienzo.render(escena,camara);
}

animar();

//Renderizar: Proceso de dibujar geometrias en la pantalla

//let step=0;

//step+=0.04;

//miCubo.position.x=20+(10*(Math.cos(step)));
//miCubo.position.y=2+(10-Math.abs(Math.sin(step)));